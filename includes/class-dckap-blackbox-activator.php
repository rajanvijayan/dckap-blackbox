<?php

/**
 * Fired during plugin activation
 *
 * @link       https://rajanvijayan.com/
 * @since      1.0.0
 *
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/includes
 * @author     Rajan Vijayan <me@rajanvijayan.com>
 */
class Dckap_Blackbox_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
