<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://rajanvijayan.com/
 * @since      1.0.0
 *
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/includes
 * @author     Rajan Vijayan <me@rajanvijayan.com>
 */
class Dckap_Blackbox_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
