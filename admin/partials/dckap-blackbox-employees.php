<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://rajanvijayan.com/
 * @since      1.0.0
 *
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
    <h1 class="wp-heading-inline">Employees</h1>
</div>
