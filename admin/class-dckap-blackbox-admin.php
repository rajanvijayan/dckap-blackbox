<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://rajanvijayan.com/
 * @since      1.0.0
 *
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/admin
 * @author     Rajan Vijayan <me@rajanvijayan.com>
 */
class Dckap_Blackbox_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dckap_Blackbox_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dckap_Blackbox_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dckap-blackbox-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Dckap_Blackbox_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Dckap_Blackbox_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dckap-blackbox-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register user roles.
	 *
	 * @since    1.0.0
	 */
	public function register_user_roles() {

		/**
		 * Create user roles based on requirements 
		 * 
		 * BlackBox Administrator
		 * Human Resource Manager
		 * Team Manager
		 * Team Lead 
		 * Candidate
		 * Employee
		 * 
		 * add_role( string $role, string $display_name, bool[] $capabilities = array() )
		 * 
		 */

		 $roles = array(
			 array( 'bba', 'BlackBox Administrator', array() ),
			 array( 'hr', 'Human Resource Manager', array() ),
			 array( 'teammanager', 'Team Manager', array() ),
			 array( 'teamlead', 'Team Lead', array() ),
			 array( 'candidate', 'Candidate', array() ),
			 array( 'employee', 'Employee', array() ),
		 );

		 foreach( $roles as $role ){
			 add_role( $role[0], $role[1], $role[2] );
		 } 

	}

	/**
	 * Add admin menu.
	 *
	 * @since    1.0.0
	 */
	public function add_menu_pages() {

		/**
		 * Create admin menu based on roles 
		 * 
		 * add_menu_page( string $page_title, string $menu_title, string $capability, 
		 * string $menu_slug, callable $function = '', string $icon_url = '', int $position = null )
		 * 
		 */

		add_menu_page(
			__( 'Black Box', 'dckap-blackbox' ),
			'BlackBox',
			'manage_options',
			'blackbox_dashboard',
			array( $this, 'blackbox_dashboard_page' ),
			'dashicons-airplane',
			3
		);

		add_submenu_page(
			'blackbox_dashboard', 
			__( 'Employees', 'dckap-blackbox' ),
			__( 'Employees', 'dckap-blackbox' ),
			'manage_options', 
			'blackbox_dashboard_employees', 
            array( $this, 'blackbox_dashboard_employees_page' )
		);
		
		add_submenu_page(
			'blackbox_dashboard', 
			__( 'Candidates', 'dckap-blackbox' ),
			__( 'Candidates', 'dckap-blackbox' ),
			'manage_options', 
			'blackbox_dashboard_candidates', 
            array( $this, 'blackbox_dashboard_candidates_page' )
		);
		
		add_submenu_page(
			'blackbox_dashboard', 
			__( 'Hire Requests', 'dckap-blackbox' ),
			__( 'Hire Requests', 'dckap-blackbox' ),
			'manage_options', 
			'blackbox_dashboard_hire_requests', 
            array( $this, 'blackbox_dashboard_hire_requests_page' )
		);
		
		add_submenu_page(
			'blackbox_dashboard', 
			__( 'Interviews', 'dckap-blackbox' ),
			__( 'Interviews', 'dckap-blackbox' ),
			'manage_options', 
			'blackbox_dashboard_interviews', 
            array( $this, 'blackbox_dashboard_interviews_page' )
        );

	}

	public function blackbox_dashboard_page(){
		if ( is_file( plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox.php' ) ) {
            include_once plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox.php';
        }
	}

	public function blackbox_dashboard_employees_page(){
		if ( is_file( plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-employees.php' ) ) {
            include_once plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-employees.php';
        }
	}

	public function blackbox_dashboard_candidates_page(){
		if ( is_file( plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-candidates.php' ) ) {
            include_once plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-candidates.php';
        }
	}

	public function blackbox_dashboard_hire_requests_page(){
		if ( is_file( plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-hire-requests.php' ) ) {
            include_once plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-hire-requests.php';
        }
	}

	public function blackbox_dashboard_interviews_page(){
		if ( is_file( plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-interviews.php' ) ) {
            include_once plugin_dir_path( __FILE__ ) . 'partials/dckap-blackbox-interviews.php';
        } 
	}

}
