## About BlackBox:

Black boxes are normally referred to by aviation experts as electronic flight data recorders. Their role is to keep detailed track of on-flight information, recording all flight data such as altitude, position and speed as well as all pilot conversations.

Likewise, DCKAP BlackBox will be recording all the information related to interviews, candidates, employees, training, and so on.

Blackbox will help us to manage the day-to-day HR activities effectively and in one place. It leverages the power of automation allowing us to save time, cut costs, and manage the employees easily. BlackBox will handle all core HR processes from new hire requests to employee exit interviews.

### Core Features
* Manage Employees
* Employee Referrals
* Manage Interview Candidate
* Monthly Performance Review
* On Boarding
* Training (Phase 2)
* Exit Process (Phase 2)
