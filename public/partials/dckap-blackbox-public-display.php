<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://rajanvijayan.com/
 * @since      1.0.0
 *
 * @package    Dckap_Blackbox
 * @subpackage Dckap_Blackbox/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
